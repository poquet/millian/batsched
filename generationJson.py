import json
import random

# Fonction pour générer une valeur aléatoire de carbone dans une plage spécifique
def generate_random_carbon(base, margin=8):
    return random.randint(base - margin, base + margin)

# Données de base pour chaque datacenter
base_values = {"dcA": 67, "dcB": 635, "dcC": 372}
margin = 8  # Marge pour les variations aléatoires

# Initialisation de la liste des jobs
jobs = []

# Générer les jobs carbon_co2
for i in range(20):  # 20 itérations
    subtime = timestamp = 150 * (i + 1)  # Utilisation du même valeur pour subtime et timestamp
    carbon_data = {
        "dcA": generate_random_carbon(base_values["dcA"], margin),
        "dcB": generate_random_carbon(base_values["dcB"], margin),
        "dcC": generate_random_carbon(base_values["dcC"], margin)
    }
    job = {
        "id": f"job_{i}",
        "type": "carbon_co2",
        "subtime": subtime,
        "timestamp": timestamp,
        "walltime": 60,
        "res": 1,
        "profile": "carbon_co2",
        "carbon_data": carbon_data
    }
    jobs.append(job)

# Structure globale du fichier
data = {
    "nb_res": 3,
    "jobs": jobs,
    "profiles": {
        "carbon_co2": {
            "type": "delay",
            "delay": 0.0001
        }
    }
}

# Définir le nom de fichier
filename = 'datacenterBatsim.json'

# Écriture des données JSON dans le fichier
with open(filename, 'w') as file:
    json.dump(data, file, indent=4)

print(f"Le fichier JSON a été généré : {filename}")
