#!/bin/bash

# Création du répertoire si nécessaire.
mkdir -p /tmp/batsim/exec

# Nettoyage du contenu du répertoire s'il y en a.
rm -rf /tmp/batsim/exec/*

# Exécution de Batsim avec l'option pour transmettre les événements inconnus
batsim -p /tmp/batsim/host/simgrid.xml \
       -w /tmp/batsim/host/datacenter-batsim.json \
       --events /tmp/batsim/events/events.txt \
       -e "/tmp/batsim/exec/out" \
       --forward-unknown-events
