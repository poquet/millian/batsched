#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <limits>
#include <nlohmann/json.hpp>
//Author Karim/Adem
using json = nlohmann::json;

struct ProfileInfo {
    std::string id;
    std::string datacenter;
    int year;
    int carbon_emission;
};

std::string extractDatacenterFromId(const std::string& id) {
    size_t pos = id.find_last_of('_');
    return pos != std::string::npos ? id.substr(0, pos) : "";
}

int extractYearFromId(const std::string& id) {
    size_t pos = id.find_last_of('_');
    return pos != std::string::npos ? std::stoi(id.substr(pos + 1)) : -1;
}

ProfileInfo findOptimalEmissionByYear(const std::vector<ProfileInfo>& profiles, int year) {
    ProfileInfo optimalProfile;
    int minEmission = std::numeric_limits<int>::max();

    for (const auto& profile : profiles) {
        if (profile.year == year && profile.carbon_emission < minEmission) {
            minEmission = profile.carbon_emission;
            optimalProfile = profile;
        }
    }

    return optimalProfile;
}

ProfileInfo findGlobalOptimalEmission(const std::vector<ProfileInfo>& profiles) {
    ProfileInfo optimalProfile;
    int minEmission = std::numeric_limits<int>::max();

    for (const auto& profile : profiles) {
        if (profile.carbon_emission < minEmission) {
            minEmission = profile.carbon_emission;
            optimalProfile = profile;
        }
    }

    return optimalProfile;
}

int main() {
    std::string filePath = "jsonInput.json";
    std::ifstream inputFile(filePath);
    if (!inputFile.is_open()) {
        std::cerr << "Erreur lors de l'ouverture du fichier : " << filePath << std::endl;
        return -1;
    }

    json jsonData;
    inputFile >> jsonData;

    std::vector<ProfileInfo> profiles;

    for (auto& profile : jsonData["profiles"].items()) {
        std::string emissionStr = profile.value()["carbon_emission"].get<std::string>();
        int emission = std::stoi(emissionStr.substr(0, emissionStr.size() - 2)); // Enlever "kg" et convertir en entier

        ProfileInfo pInfo;
        pInfo.id = profile.key();
        pInfo.datacenter = extractDatacenterFromId(pInfo.id);
        pInfo.year = extractYearFromId(pInfo.id);
        pInfo.carbon_emission = emission;
        profiles.push_back(pInfo);
    }

    int choice;
    std::cout << "Choisissez une option :\n1. Trouver l'empreinte carbone la plus faible pour une annÃ©e spécifique\n2. Trouver l'empreinte carbone globale la plus faible\nVotre choix : ";
    std::cin >> choice;

    if (choice == 1) {
        int year;
        std::cout << "Entrez l'année : ";
        std::cin >> year;
        ProfileInfo optimalProfile = findOptimalEmissionByYear(profiles, year);
        if (!optimalProfile.id.empty()) {
            std::cout << "L'empreinte carbone la plus faible pour " << year << " est en " << optimalProfile.datacenter << " : " << optimalProfile.carbon_emission << "kg\n";
        } else {
            std::cout << "Aucune empreinte carbone trouvée pour l'année " << year << ".\n";
        }
    } else if (choice == 2) {
        ProfileInfo globalOptimalProfile = findGlobalOptimalEmission(profiles);
        if (!globalOptimalProfile.id.empty()) {
            std::cout << "L'empreinte carbone globale la plus faible est :\n"
                      << "Datacenter: " << globalOptimalProfile.datacenter << ", AnnÃ©e: " << globalOptimalProfile.year
                      << ", Empreinte carbone: " << globalOptimalProfile.carbon_emission << "kg\n";
        } else {
            std::cout << "Aucune empreinte carbone globale optimale trouvée.\n";
        }
    } else {
        std::cout << "Option invalide.\n";
    }

    return 0;
}
