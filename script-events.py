import json

# Charger le fichier JSON source
with open('datacenterBatsim.json', 'r') as file:
    data = json.load(file)

# Ouvrir le fichier de sortie .txt
with open('events.txt', 'w') as file:
    # Parcourir chaque job dans le fichier JSON
    for job in data["jobs"]:
        # Créer un dictionnaire pour l'événement basé sur les données du job
        event = {
            "type": job["type"],
            "timestamp": job["timestamp"],
            "some_field_dcA": job["carbon_data"]["dcA"],  # Données pour dcA
            "another_field_dcB": job["carbon_data"]["dcB"],  # Données pour dcB
            "additional_field_dcC": job["carbon_data"]["dcC"]  # Données pour dcC
        }
        # Convertir l'événement en chaîne JSON et l'écrire dans le fichier, une ligne par événement
        event_str = json.dumps(event)
        file.write(event_str + "\n")

print("Le fichier d'événements a été généré : events.txt")
