#ifndef CARBONE_HPP
#define CARBONE_HPP
#pragma once

#include <list>
#include "../isalgorithm.hpp"
#include "../json_workload.hpp"
#include "../locality.hpp"
#include "../schedule.hpp"
#include "isalgorithm.hpp" 

class CarboneAlgorithm : public ISchedulingAlgorithm {
public:
    CarboneAlgorithm(Workload * workload, SchedulingDecision * decision, Queue * queue, ResourceSelector * selector, double rjms_delay, rapidjson::Document * variant_options);
    virtual ~CarboneAlgorithm();

    virtual void on_simulation_start(double date, const rapidjson::Value & batsim_config);
    virtual void on_simulation_end(double date);
    void make_decisions(double date, SortableJobOrder::UpdateInformation *update_info, SortableJobOrder::CompareInformation *compare_info) override;
    void updateCarbonFootprint(int dcA, int dcB, int dcC);

private:
    int select_datacenter_with_lowest_carbon_footprint();
    int carbonFootprintDcA;
    int carbonFootprintDcB;
    int carbonFootprintDcC;
protected:
    Schedule _schedule;
    bool _debug = false;
};

#endif // CARBONE_HPP
