{"type": "datacenter_announcement", "id": "dcA", "location": "France", "year": 2018, "carbon_intensity": 67, "timestamp": 0}
{"type": "datacenter_announcement", "id": "dcA", "location": "France", "year": 2019, "carbon_intensity": 69, "timestamp": 1}
{"type": "datacenter_announcement", "id": "dcA", "location": "France", "year": 2020, "carbon_intensity": 67, "timestamp": 2}
{"type": "datacenter_announcement", "id": "dcA", "location": "France", "year": 2021, "carbon_intensity": 67, "timestamp": 3}
{"type": "datacenter_announcement", "id": "dcC", "location": "United States", "year": 2021, "carbon_intensity": 379, "timestamp": 4}
{"type": "datacenter_announcement", "id": "dcC", "location": "United States", "year": 2020, "carbon_intensity": 369, "timestamp": 5}
{"type": "datacenter_announcement", "id": "dcC", "location": "United States", "year": 2019, "carbon_intensity": 393, "timestamp": 6}
{"type": "datacenter_announcement", "id": "dcC", "location": "United States", "year": 2018, "carbon_intensity": 412, "timestamp": 7}
{"type": "datacenter_announcement", "id": "dcB", "location": "Morocco", "year": 2021, "carbon_intensity": 631, "timestamp": 8}
{"type": "datacenter_announcement", "id": "dcB", "location": "Morocco", "year": 2020, "carbon_intensity": 642, "timestamp": 9}
{"type": "datacenter_announcement", "id": "dcB", "location": "Morocco", "year": 2019, "carbon_intensity": 627, "timestamp": 10}
{"type": "datacenter_announcement", "id": "dcB", "location": "Morocco", "year": 2018, "carbon_intensity": 627, "timestamp": 11}
