[![pipeline status](https://framagit.org/batsim/batsched/badges/master/pipeline.svg)](https://framagit.org/batsim/batsched/pipelines)
[![coverage](https://img.shields.io/codecov/c/github/oar-team/batsched.svg)](https://codecov.io/gh/oar-team/batsched)
[![changelog](https://img.shields.io/badge/doc-changelog-blue.svg)](./CHANGELOG.md)

**batsched** is a set of [Batsim]-compatible algorithms implemented in C++.

## Development setup prerequisites
1. Install Nix with the `nix` command and nix `flakes` enabled.
   This can be done via [nix-portable](https://github.com/DavHau/nix-portable) or with a [full install](https://nixos.org/download) + [configuration](https://nixos.wiki/wiki/Flakes#Other_Distros.2C_without_Home-Manager).

## Dev workflow 1 (pure build every time)
1. Hack some input files.
2. (if you have added new files to Git that are needed to compile batsched, you must `git add` them)
3. Build batsched via `nix`: `nix build .#batsched` from the git repository's root.
4. Use the `batsched` that has been compiled. It is available in `./result/bin/batsched`

## Dev workflow 2 (iterative builds)
1. Hack some input files.
2. Enter a shell that you'll use to compile batsched: `nix develop .#batsched`.
   In this shell you can compile batsched how you'd do without Nix:
   - `meson setup build` once
   - then `ninja -C build` everytime you want to compile batsched
3. Use the `batsched` that has been compiled. It is available in `./build/batsched`
