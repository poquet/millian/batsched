import json
import random

# Fonction pour générer une valeur aléatoire de carbone dans une plage spécifique
def generate_random_carbon(base, margin=8):
    return random.randint(base - margin, base + margin)

# Données de base pour chaque pays
base_values = {"France": 67, "Morocco": 635, "USA": 372}
margin = 8  # Marge pour les variations aléatoires

# Initialisation de la liste des événements
events = []

# Générer les événements carbon_co2
for i in range(20):  # 20 itérations
    timestamp = 150 * (i + 1)
    carbon_data = {
        "dcA": generate_random_carbon(base_values["France"], margin),
        "dcB": generate_random_carbon(base_values["Morocco"], margin),
        "dcC": generate_random_carbon(base_values["USA"], margin)
    }
    event = {
        "type": "carbon_co2",
        "timestamp": timestamp,
        "carbon_data": carbon_data
    }
    events.append(event)

# Définir le nom de fichier
filename = 'datacenterBatsim.json'

# Écriture des données JSON dans le fichier
with open(filename, 'w') as file:
    json.dump(events, file, indent=4)

print(filename)
